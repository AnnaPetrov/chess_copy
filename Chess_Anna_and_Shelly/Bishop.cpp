#include "Bishop.h"
#include "Board.h"

Bishop::Bishop(const bool isWhite, Board& board):
	Piece(isWhite, board)
{
	if (this->_isWhite)
	{
		this->_name = BISHOP_NAME_WHITE;
	}
	else
	{
		this->_name = BISHOP_NAME_BLACK;
	}
}

Bishop::~Bishop()
{
}

/*Checks if bishop can move from given src to given dst.
* Bishop moves in diagonal lines, with same hist to each side
* Input: src to move from, dst to move to
* Output: can bishop move like this?
*/
bool Bishop::isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const
{
	return this->isLegalMove(this->_board, srcCol, srcRow, dstCol, dstRow);
}

//Same function but static and with board as parameter
//Boat=rd is the board the bishop moves on
bool Bishop::isLegalMove(const Board* board, const int srcCol, const int srcRow, const int dstCol, const int dstRow)
{
	bool flag = true;
	int hist = 0, end = 0, colDirection = 0, rowDirection = 0;
	while (flag)
	{
		hist = dstCol - srcCol;
		// check if move is made in diagonal line
		if (std::abs(dstRow - srcRow) != std::abs(hist))
		{
			flag = false;
			break;
		}
		hist < 0 ? colDirection = BACKWARD_MOVE : colDirection = FORWARD_MOVE;
		dstRow - srcRow < 0 ? rowDirection = BACKWARD_MOVE : rowDirection = FORWARD_MOVE;
		//check for other pieces in the way
		end = std::abs(hist);
		for (size_t i = PREV_PIECE; i < end; i++)
		{
			if (board->getPiece(srcRow + rowDirection * i, srcCol + colDirection * i).getName() != NULL_PIECE_NAME)
			{
				flag = false;
				break;
			}
		}
		//check dst piece: 
		break;
	}
	return flag;
}