#pragma once
#ifndef BISHOP_H
#define BISHOP_H

#include "Piece.h"

class Bishop : public Piece
{
public:
	//Constructor and destructor
	Bishop(const bool isWhite, Board& board);
	virtual ~Bishop();

	//other
	virtual bool isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const override;
	static  bool isLegalMove(const Board* board, const int srcCol, const int srcRow, const int dstCol, const int dstRow);
};
#endif