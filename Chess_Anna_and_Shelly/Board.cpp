#include "Board.h"
#include <iostream>
#include "Exceptions.h"

//  Constructor: builds classic chess board
//	rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR
Board::Board()
{
	int row = 0;
	int col = 0;
	int boardSize = BOARD_SIZE - 1;

	this->_lastEatten = nullptr;
	this->_blackKing = new King(BLACK, *this);
	this->_whiteKing = new King(WHITE, *this);

	//init board:
	for (col = 0; col < BOARD_SIZE; col++)
	{
		//init black pawns
		this->_board[BLACK_PAWNS_ROW][col] = new Pawn(BLACK, *this);
		//init white pawns
		this->_board[WHITE_PAWNS_ROW][col] = new Pawn(WHITE, *this);
	}

	//init other black pieces:
	this->_board[BLACK_PIECES_ROW][ROOK_ID] = new Rook(BLACK, *this);
	this->_board[BLACK_PIECES_ROW][boardSize - ROOK_ID] = new Rook(BLACK, *this);
	this->_board[BLACK_PIECES_ROW][KNIGHT_ID] = new Knight(BLACK, *this);
	this->_board[BLACK_PIECES_ROW][boardSize - KNIGHT_ID] = new Knight(BLACK, *this);
	this->_board[BLACK_PIECES_ROW][BISHOP_ID] = new Bishop(BLACK, *this);
	this->_board[BLACK_PIECES_ROW][boardSize - BISHOP_ID] = new Bishop(BLACK, *this);
	this->_board[BLACK_PIECES_ROW][KING_ID] = this->_blackKing;
	this->_board[BLACK_PIECES_ROW][boardSize - QUEEN_ID] = new Queen(BLACK, *this);

	//init other white pieces:
	this->_board[WHITE_PIECES_ROW][ROOK_ID] = new Rook(WHITE, *this);
	this->_board[WHITE_PIECES_ROW][boardSize - ROOK_ID] = new Rook(WHITE, *this);
	this->_board[WHITE_PIECES_ROW][KNIGHT_ID] = new Knight(WHITE, *this);
	this->_board[WHITE_PIECES_ROW][boardSize - KNIGHT_ID] = new Knight(WHITE, *this);
	this->_board[WHITE_PIECES_ROW][BISHOP_ID] = new Bishop(WHITE, *this);
	this->_board[WHITE_PIECES_ROW][boardSize - BISHOP_ID] = new Bishop(WHITE, *this);
	this->_board[WHITE_PIECES_ROW][KING_ID] = this->_whiteKing;
	this->_board[WHITE_PIECES_ROW][boardSize - QUEEN_ID] = new Queen(WHITE, *this);

	//Other are null pieces
	for (row = WHITE_PAWNS_ROW + 1; row < BLACK_PAWNS_ROW; row++)
	{
		for (col = 0; col < BOARD_SIZE; col++)
		{
			this->_board[row][col] = new NullPiece(NULL_P_COLOR, *this);
		}
	}
}

//destructor
Board::~Board()
{
	for (int row = 0; row < BOARD_SIZE; row++)
	{
		for (int col = 0; col < BOARD_SIZE; col++)
		{
			delete this->_board[row][col]; //delete each piece one by one (including the kings)
		}
	}
	if (this->_lastEatten != nullptr)// ensure all pieces are deleted
	{
		delete this->_lastEatten;
	}
}

/* Checks if piece can move:
* dst and src are on board, moved piece isn't null, not eating piece with same color, the move is legal
* Input: dst to move to and src to move from
* Output: can this move be made?
*/
bool Board::isValidMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const
{
	Piece* srcPiece = &this->getPiece(srcRow, srcCol);
	Piece* dstPiece = &this->getPiece(dstRow, dstCol);

	//src and dst checked, valid indexes
	if (!(*srcPiece))
	{
		//exception: moving empty piece
		throw ExceptionWrongSource();
	}
	else if (!(!(*dstPiece)) && *srcPiece == *dstPiece)
	{
		throw ExceptionSelfAttack(); //trying to eat piece with the same color
	}
	return srcPiece->isLegalMove(srcCol, srcRow, dstCol, dstRow);
}

/*Moves piece if it's possible, otherways throws exception
* Input: dst to move to and src to move from
* Output: none, or exception
*/
bool Board::movePiece(const int srcCol, const int srcRow, const int dstCol, const int dstRow)
{
	if (this->isValidMove(srcCol, srcRow, dstCol, dstRow))
	{
		if (this->_lastEatten)
		{
			delete this->_lastEatten;
		}
		this->_lastEatten = this->_board[dstRow][dstCol];
		//moving piece to destination
		this->_board[dstRow][dstCol] = this->_board[srcRow][srcCol];
		this->_board[srcRow][srcCol] = new NullPiece(NULL_P_COLOR, *this);
		return true;
	}
	else
	{
		return false;
	}
}

/*Restores previous board state
* Input: the last made move indexes
*/
void Board::restoreMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow)
{
	if (this->_lastEatten != this->_board[dstRow][dstCol] && this->_lastEatten != nullptr)
	{
		delete(this->_board[srcRow][srcCol]); //delete null piece
		this->_board[srcRow][srcCol] = this->_board[dstRow][dstCol]; //replace
		this->_board[dstRow][dstCol] = this->_lastEatten; //replace
		this->_lastEatten = nullptr;
	}
}

//Getters:

Piece& Board::getPiece(const int row, const int col) const
{
	//check if on board
	if (row > BOARD_SIZE || col > BOARD_SIZE || row < 0 || col < 0)
	{
		throw ExceptionWrongIndex();
	}
	return *this->_board[row][col];
}

King& Board::getWhiteKing() const
{
	return *this->_whiteKing;
}

King& Board::getBlackKing() const
{
	return *this->_blackKing;
}

//Returns string with all pieces on board
std::string Board::getBoard() const
{
	std::string boardPieces = "";
	for (int row = BOARD_SIZE - 1; row >= 0; row--)
	{
		for (int col = 0; col < BOARD_SIZE; col++)
		{
			boardPieces += this->_board[row][col]->getName();
		}
	}
	return boardPieces;
}

/*Prints the board on console:
* Prints each piece name
*/
void Board::printBoard() const
{
	std::cout << "    a b c d e f g h" << std::endl << std::endl;
	for (int i = BOARD_SIZE - 1; i >= 0; i--)
	{
		std::cout << (i + 1) << "   ";
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			std::cout << this->_board[i][j]->getName() << " ";
		}
		std::cout << std::endl;
	}
}

