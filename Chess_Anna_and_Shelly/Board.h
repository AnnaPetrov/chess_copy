#pragma once
#ifndef BOARD_H
#define BOARD_H
//all pieces:
#include "Piece.h"
#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "NullPiece.h"
#include "Pawn.h"
#include "Rook.h"
#include "Queen.h"

#include "Defines.h"
#include <iostream>
#include <vector>

//remove "undefined class" error:

class Piece;
class Bishop;
class King;
class Knight;
class NullPiece;
class Pawn;
class Rook;
class Queen;

class Board
{
private:
	Piece* _board[BOARD_SIZE][BOARD_SIZE];
	Piece* _lastEatten;
	King* _whiteKing;
	King* _blackKing;
public:
	//Constructor and destructor:
	Board();
	virtual ~Board();
	//getters
	std::string getBoard() const;
	Piece& getPiece(const int row, const int col) const;
	King& getWhiteKing() const;
	King& getBlackKing() const;
	//other:
	bool movePiece(const int srcCol, const int srcRow, const int dstCol, const int dstRow);
	void restoreMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow);
	void printBoard() const;
	bool isValidMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const;
};

#endif