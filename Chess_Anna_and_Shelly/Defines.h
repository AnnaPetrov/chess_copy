#pragma once

#include <iostream>

//Board Constants:
#define BOARD_SIZE 8
#define WHITE true
#define BLACK false
#define NULL_P_COLOR false

//Manegaer constants:
#define MOVE_SIZE 4
#define RECONNECT "0"
#define WHITE_PLAYER "0"
#define BLACK_PLAYER "1"
#define STRING_LAST_CHAR '\0'
#define QUIT "quit"
#define BUFFER_SIZE 1024


//Piece names:
#define NULL_PIECE_NAME '#'

#define BISHOP_NAME_BLACK 'b'
#define BISHOP_NAME_WHITE 'B'

#define KING_NAME_BLACK 'k'
#define KING_NAME_WHITE 'K'

#define KNIGHT_NAME_BLACK 'n'
#define KNIGHT_NAME_WHITE 'N'

#define PAWN_NAME_BLACK 'p'
#define PAWN_NAME_WHITE 'P'

#define QUEEN_NAME_WHITE 'Q'
#define QUEEN_NAME_BLACK 'q'

#define ROOK_NAME_BLACK 'r'
#define ROOK_NAME_WHITE 'R'

//Codes:
#define OK '0'
#define OPPONENT_CHESSED '1'
#define WRONG_SOURCE '2'
#define SELF_ATTACK '3'
#define SELF_CHESS '4'
#define WRONG_ID '5'
#define ILLEGAL_MOVE '6'
#define MOVING_TO_SAME_PLACE '7'
#define WIN '8'

//Board indexes:
#define SRC_COL 0
#define SRC_ROW 1
#define DST_COL 2
#define DST_ROW 3

//piece indexes:
#define ROOK_ID 0
#define KNIGHT_ID 1
#define BISHOP_ID 2
#define KING_ID 3
#define QUEEN_ID 3

#define BLACK_PAWNS_ROW 6
#define WHITE_PAWNS_ROW 1
#define BLACK_PIECES_ROW 7
#define WHITE_PIECES_ROW 0

//ASCII offsets
#define ASCII_NUM_OFFSET 49
#define ASCII_LETTER_OFFSET 97

//Piece movement:
#define KING_MAX_MOVE 1

#define KNIGHT_SIDE1_MOVE 1
#define KNIGHT_SIDE2_MOVE 2

#define PAWN_REGULAR_FORWARD_MOVE 1
#define PAWN_FIRST_FORWARD_MOVE 2
#define PAWN_DIAGINAL_EAT_MOVE 1
#define PREV_PIECE 1

#define FORWARD_MOVE 1
#define BACKWARD_MOVE -1