#include "Exceptions.h"
#define BUFFER_SIZE 20 
#define ASCII_OF_a int('a')
#pragma warning(disable : 4996)//STRCAT WARNING

const char* ExceptionWrongSource::what(int srcCol, int srcRow) const
{
	char fullMsg[BUFFER_SIZE] = { '\0' };
	// description about cell
	const char msg[] = "There's no piece at ";
	// cell index
	const char cell[] = { char(srcRow), char((srcCol + ASCII_OF_a)) };
	strcpy(fullMsg, msg);
	strcat(fullMsg, cell);
	return fullMsg;
}

const char* ExceptionSelfAttack::what(int srcCol, int srcRow) const
{
	return "Destination cell contains piece of current player!";
}

const char* ExceptionSelfChess::what(int srcCol, int srcRow) const
{
	return "This move will cause self- chessing!";
}

const char* ExceptionWrongIndex::what(int srcCol, int srcRow) const
{
	char fullMsg[BUFFER_SIZE + BUFFER_SIZE] = { '\0' };
	// word "index"
	const char index[] = "Index ";
	strcpy(fullMsg, index);
	// actual index
	const char cell[] = { char(srcRow), char((srcCol + ASCII_OF_a)) };
	strcat(fullMsg, cell);
	// description about index
	const char msg[] = " is out of range!";
	strcat(fullMsg, msg);
	return fullMsg;
}

const char* ExceptionIllegalMove::what(int srcCol, int srcRow, int dstCol, int dstRow) const
{
	char fullMsg[BUFFER_SIZE + BUFFER_SIZE] = { '\0' };
	// word "index"
	const char msg1[] = "Piece can't move from cell ";
	strcpy(fullMsg, msg1);
	// actual index
	const char cell1[] = { char(srcRow), char((srcCol + int('a'))) };
	strcat(fullMsg, cell1);
	const char msg2[] = " to cell ";
	strcat(fullMsg, msg2);
	// actual index
	const char cell2[] = { char(dstRow), char((dstCol + int('a'))) };
	strcat(fullMsg, cell2);
	return fullMsg;
}

const char* ExceptionSameSrcDest::what(int srcCol, int srcRow) const
{
	return "Source and destination cells are similiar!";
}
