#pragma once
#include <string>
class ExceptionWrongSource : public std::exception
{
public:
	virtual const char* what(int srcCol, int srcRow) const;
};

class ExceptionSelfAttack : public std::exception
{
public:
	virtual const char* what(int srcCol, int srcRow) const;
};

class ExceptionSelfChess : public std::exception
{
public:
	virtual const char* what(int srcCol, int srcRow) const;
};

class ExceptionWrongIndex : public std::exception
{
public:
	virtual const char* what(int srcCol, int srcRow) const;
};

class ExceptionIllegalMove : public std::exception
{
public:
	virtual const char* what(int srcCol, int srcRow, int dstCol, int dstRow) const;
};

class ExceptionSameSrcDest : public std::exception
{
public:
	virtual const char* what(int srcCol, int srcRow) const;
};