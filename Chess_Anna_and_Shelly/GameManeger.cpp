#include "GameManeger.h"
#include "Exceptions.h"
#include "Defines.h"
//Constructor
GameManeger::GameManeger()
{
	this->_graphics = new Pipe();
	this->_board = new Board();
	this->_currentPlayer = new Player(*this->_board, WHITE, this->_board->getWhiteKing()); //white figures play first
	this->_otherPlayer = new Player(*this->_board, BLACK, this->_board->getBlackKing());
}

//Destructor
GameManeger::~GameManeger()
{
	delete(this->_currentPlayer);
	delete(this->_otherPlayer);
	delete(this->_board);
	delete(this->_graphics);
}

/*Connects game manager to graphics
*/
bool GameManeger::connectToGraphics() const
{
	bool isConnected = false;
	string ans = "";
	do
	{
		isConnected = this->_graphics->connect();
		if (!isConnected)
		{
			cout << "cant connect to graphics" << endl;
			cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
			std::cin >> ans;
			if (ans == RECONNECT)
			{
				cout << "trying connect again.." << endl;
				Sleep(5000);
			}
			else
			{
				this->_graphics->close();
				break;
			}
		}
	} while (!isConnected);
	return isConnected;
}

/*
This function does basic game managemant: connects to graphics and recives messages from it
*/
void GameManeger::manageGame()
{
	bool isConnect = this->connectToGraphics();
	char msgToGraphics[BUFFER_SIZE];
	string msgFromGraphics = "";
	std::string firstPlayer = WHITE_PLAYER;
	
	if (!this->_currentPlayer->isWhite())
	{
		firstPlayer = BLACK_PLAYER;
	}
	// first msgToGraphics should contain the board string accord the protocol
	//"rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"
	strcpy_s(msgToGraphics, (this->_board->getBoard() + firstPlayer).c_str()); //first message
	
	//connection to graphics loop
	do
	{
		this->_graphics->sendMessageToGraphics(msgToGraphics);

		msgFromGraphics = this->_graphics->getMessageFromGraphics();
		if (msgFromGraphics == QUIT)
		{
			break;
		}
		char code[] = { this->getCode(msgFromGraphics) , STRING_LAST_CHAR };
		strcpy_s(msgToGraphics, code); // msgToGraphics should contain the result of the operation

	} while (msgFromGraphics != QUIT);
	this->_graphics->close();
}

/*This function checksa recived move from graphics and returns the code:
* Was the move ok? If not, what was wrong?
* Input: message from graphics
* Output: the coe to send to graphics
*/
char GameManeger::getCode(const std::string msgFromGraphics)
{
	char move[MOVE_SIZE] = "";
	char code = OK;

	if (msgFromGraphics.size() != MOVE_SIZE)
	{
		exit(1); //something went wrong
	}

	//convert message values to indexes:
	move[SRC_COL] = msgFromGraphics[SRC_COL] - ASCII_LETTER_OFFSET;
	move[SRC_ROW] = msgFromGraphics[SRC_ROW] - ASCII_NUM_OFFSET;
	move[DST_COL] = msgFromGraphics[DST_COL] - ASCII_LETTER_OFFSET;
	move[DST_ROW] = msgFromGraphics[DST_ROW] - ASCII_NUM_OFFSET;

	try
	{
		//try to move:
		//wrong movement leads to exceptions
		this->_currentPlayer->movePiece(move[SRC_COL], move[SRC_ROW], move[DST_COL], move[DST_ROW]);

		if (this->_otherPlayer->isChessed())
		{
			if (this->_otherPlayer->isCheckmated())
			{
				code = WIN;
			}
			else
			{
				code = OPPONENT_CHESSED;
			}
		}
		Player* tmp = this->_otherPlayer;
		this->_otherPlayer = this->_currentPlayer;
		this->_currentPlayer = tmp;
	}
	catch (ExceptionWrongSource exception)//2
	{
		code = WRONG_SOURCE;
	}
	catch (ExceptionSelfAttack exception)//3
	{
		code = SELF_ATTACK;
	}
	catch (ExceptionSelfChess exception)//4
	{
		code = SELF_CHESS;
	}
	catch (ExceptionWrongIndex exception)//5
	{
		code = WRONG_ID;
	}
	catch (ExceptionIllegalMove exception)//6
	{
		code = ILLEGAL_MOVE;
	}
	catch (ExceptionSameSrcDest exception)//7
	{
		code = MOVING_TO_SAME_PLACE;
	}
	catch (const std::exception& e)//default
	{
		std::cerr << "ERROR: " << e.what();
	}
	this->_board->printBoard();
	return code;
}
