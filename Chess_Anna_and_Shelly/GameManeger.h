#pragma once
#include "Board.h"
#include "Player.h"
#include "Pipe.h"
#include "Defines.h"

#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::string;

class GameManeger 
{
private:
	Board* _board;
	Player* _currentPlayer;
	Player* _otherPlayer;
	Pipe* _graphics;

	//helpers:
	bool connectToGraphics() const;

public:
	//Constructor and destructor:
	GameManeger();
	virtual ~GameManeger();
	//getter
	char getCode(const std::string msgFromGraphics);
	//other:
	void manageGame();
	};