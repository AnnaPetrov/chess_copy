#include "King.h"
#include "Board.h"

King::King(const bool isWhite, Board& board):
	Piece(isWhite, board)
{
	if (this->_isWhite)
	{
		this->_name = KING_NAME_WHITE;
	}
	else
	{
		this->_name = KING_NAME_BLACK;
	}
}

King::~King()
{
}

/*Checks if king is able to move to given place.
* King can move only 1 step to each direction
* Input: king id and dst id
* Output: can the king move to given place?
*/
bool King::isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const
{
	int stepsRow = std::abs(srcCol - dstCol);
	int stepsCol = std::abs(srcRow - dstRow);
	if ((stepsRow <= KING_MAX_MOVE) && (stepsCol<= KING_MAX_MOVE))
	{
		return true; //only one step to each side allowed
	}
	return false;
}

/*Checks if this king is checkmated. King is checkmated only if there is no move to prottect the king
* Checks all posssible moves if they can prottect the king from checkmate
* Output: is king checkmated?
*/
bool King::isCheckmated() const
{
	bool isCheckmated = true;
	//check if there is move to prottect the king
	for (int srcRow = 0; srcRow < BOARD_SIZE; srcRow++)
	{
		for (int srcCol = 0; srcCol < BOARD_SIZE; srcCol++)
		{
			Piece* srcPiece = &this->_board->getPiece(srcRow, srcCol);
			//check each piece with same color as king if it can prottect the king
			if (!(!(*srcPiece)) && *srcPiece == *this)
			{
				for (int dstRow = 0; dstRow < BOARD_SIZE; dstRow++)
				{
					for (int dstCol = 0; dstCol < BOARD_SIZE; dstCol++)
					{
						Piece* dstPiece = &this->_board->getPiece(dstRow, dstCol);
						//piece can only attack other piece or move to empty place 
						if (!(*dstPiece) || *srcPiece != *dstPiece)
						{
							//check if src piece can prottect the king
							if (this->_board->movePiece(srcCol, srcRow, dstCol, dstRow))
							{
								if (!this->isChessed())
								{
									isCheckmated = false;
								}
								this->_board->restoreMove(srcCol, srcRow, dstCol, dstRow);
								if (!isCheckmated)
								{
									return isCheckmated; //move to prottect the king found
								}
							}
						}
					}
				}
			}
		}
	}
	//if chess loop passed, king is checkmated
	return isCheckmated;
}

/*Checks if this king is chessed. King is chessed if 1 or more pieces can attack the king
* It finds the kings place on board, than it checks each opponent piece if it can eat the king
* Output: is king chessed?
*/
bool King::isChessed() const
{
	int kingRow = 0;
	int kingCol = 0;
	//find kings place on board
	this->findPlace(&kingRow, &kingCol);
	//check if other pieces can move to kings place
	for (int row = 0; row < BOARD_SIZE; row++)
	{
		for (int col = 0; col < BOARD_SIZE; col++)
		{
			if (this->attacked(col, row, kingCol, kingRow))
			{
				return true; //one or more opponent pieces can attack the king
			}
		}
	}
	//if chess loop passed, king isn't chessed
	return false;
}