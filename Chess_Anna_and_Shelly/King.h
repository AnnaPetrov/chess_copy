#pragma once
#include "Piece.h"

class King : public Piece
{
public:
	//Constructor and destructor
	King(const bool isWhite, Board& board);
	virtual ~King();

	//other
	virtual bool isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const override;
	bool isChessed() const;
	bool isCheckmated() const;

};