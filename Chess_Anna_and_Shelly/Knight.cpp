#include "Knight.h"
#include "Board.h"
Knight::Knight(const bool isWhite, Board& board) :
	Piece(isWhite, board)
{
	if (this->_isWhite)
	{
		this->_name = KNIGHT_NAME_WHITE;
	}
	else
	{
		this->_name = KNIGHT_NAME_BLACK;
	}
}

Knight::~Knight()
{
}

/*Knight can move 2 steps to one side and 1 to other side. It can step above other pieces
* Input: knight index, dst index
* Output: can this knight move to given place?
*/
bool Knight::isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const
{
	int rowMove = std::abs(srcRow - dstRow);
	int colMove = std::abs(srcCol - dstCol);
	if (((rowMove == KNIGHT_SIDE1_MOVE) && (colMove == KNIGHT_SIDE2_MOVE)) ||
		((colMove == KNIGHT_SIDE1_MOVE) && (rowMove == KNIGHT_SIDE2_MOVE)))
	{
		return true;
	}
	return false;
}