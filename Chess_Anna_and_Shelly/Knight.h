#pragma once
#include "Piece.h"

class Knight : public Piece
{

public:
	//Constructor and destructor
	Knight(const bool isWhite, Board& board);
	virtual ~Knight();

	//other
	virtual bool isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const override;
};