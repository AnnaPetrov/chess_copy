#include "NullPiece.h"
#include "Defines.h"
NullPiece::NullPiece(const bool isWhite, Board& board):
	Piece(isWhite, board)
{
	this->_name = NULL_PIECE_NAME;
}

NullPiece::~NullPiece()
{
}

bool NullPiece::isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const
{
	return false; //null piece can't move
}
