#pragma onces
#include "Piece.h"

class NullPiece : public Piece
{

public:
	//Constructor and destructor
	NullPiece(const bool isWhite, Board& board);
	virtual ~NullPiece();

	//other
	virtual bool isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const override;
};