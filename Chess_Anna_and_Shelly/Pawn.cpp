#include "Pawn.h"
#include "Board.h"

Pawn::Pawn(const bool isWhite, Board& board):
	Piece(isWhite, board)
{
	if (this->_isWhite)
	{
		this->_name = PAWN_NAME_WHITE;
	}
	else
	{
		this->_name = PAWN_NAME_BLACK;
	}
	this->_isFirstMove = true;
}

Pawn::~Pawn()
{
}

/*Pawn can move 1 step forward or 2 if it's the first pawn move.
* If pawn eats other piece, it should do it in single diagonal step
*/
bool Pawn::isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const
{
	bool validMove = false;
	int prevStep = 0;
	int rowMove = dstRow - srcRow;
	char dstName = this->_board->getPiece(dstRow, dstCol).getName();

	if (!this->_isWhite)
	{
		rowMove = -rowMove;
	}
	if (rowMove == PAWN_REGULAR_FORWARD_MOVE)
	{
		if (dstCol == srcCol) //moving forward, not eating other piece
		{
			if (dstName == NULL_PIECE_NAME)
			{
				validMove = true;
			}
		}
		else if (std::abs(dstCol - srcCol) == PAWN_DIAGINAL_EAT_MOVE) //moving single step in diagonal, eat piece
		{
			//check if trying to eat other player:
			if (dstName != NULL_PIECE_NAME)
			{
				validMove = true;
			}
		}
	}
	else if((dstCol == srcCol) && rowMove == PAWN_FIRST_FORWARD_MOVE && this->_isFirstMove) //moving first move and 2 steps
	{
		//check if there are pieces on the way
		if (this->_isWhite)
		{
			prevStep = PREV_PIECE;
		}
		else
		{
			prevStep = -PREV_PIECE;
		}
		if (dstName == NULL_PIECE_NAME && this->_board->getPiece(srcRow + prevStep, srcCol).getName() == NULL_PIECE_NAME)
		{
			validMove = true;
		}
	}
	return validMove;
}

//setters:
void Pawn::setFirstMove(bool isFirstMove)
{
	this->_isFirstMove = isFirstMove;
}

//getters:
bool Pawn::isFirstMove() const
{
	return this->_isFirstMove;
}
