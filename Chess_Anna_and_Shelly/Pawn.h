#pragma once
#include "Piece.h"

class Pawn : public Piece
{
private:
	bool _isFirstMove;
public:
	//Constructor and destructor
	Pawn(const bool isWhite, Board& board);
	virtual ~Pawn();

	//other
	virtual bool isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const override;
	void setFirstMove(bool isFirstMove);
	bool isFirstMove() const;
};