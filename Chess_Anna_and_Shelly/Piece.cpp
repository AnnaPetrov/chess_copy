#include "Piece.h"
#include "Board.h"
Piece::Piece(const bool isWhite, Board& board)
{
	this->_isWhite = isWhite;
	this->_board = &board;
	this->_name = ' '; //empty name
}

Piece::~Piece()
{
	//destroys Piece (default)
}
//getters:

bool Piece::isWhite() const
{
	return this->_isWhite;
}

char Piece::getName() const
{
	return this->_name;
}

/*Checks if piece on src place can attck piece on dst place
* input: dst and src pieces id
* Output: can src attack dst?
*/
bool Piece::attacked(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const
{
	Piece* attackPiece = &this->_board->getPiece(srcRow, srcCol);
	if (this->_isWhite != attackPiece->isWhite() && attackPiece->isLegalMove(srcCol, srcRow, dstCol, dstRow))
	{
		//other player piece can move to this place
		return true;
	}
	return false;
}

/*Finds this piece place on board by it's name and returns row and col id in given parameters
* Input: row and col to save the id
*/
void Piece::findPlace(int* pieceRow, int* pieceCol) const
{
	for (int row = 0; row < BOARD_SIZE; row++)
	{
		for (int col = 0; col < BOARD_SIZE; col++)
		{
			if (this->_board->getPiece(row, col).getName() == this->_name)
			{
				//piece found
				*pieceCol = col;
				*pieceRow = row;
				return;
			}
		}
	}
}

//Pieces have same color
bool Piece::operator==(const Piece& other) const
{
	return this->_isWhite == other._isWhite;
}

//the opposite to == operator: pieces have different colors
bool Piece::operator!=(const Piece& other) const
{
	return !(*this==other);
}

//returns true if piece is null piece and falce if not
bool Piece::operator!() const
{
	if (this->getName() == NULL_PIECE_NAME)
	{
		return true;
	}
	return false;
}
