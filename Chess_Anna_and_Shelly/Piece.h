#pragma once
#include "Defines.h"
class Board;

//Obstract class Piece, defines basics
class Piece
{
protected:
	bool _isWhite;
	char _name;
	Board* _board;

	//helpers:
	bool attacked(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const;
	void findPlace(int* pieceRow, int* pieceCol) const;
public:
	//Constructor and destructor
	Piece(const bool isWhite, Board& board);
	virtual ~Piece();

	//other
	virtual bool isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const = 0;
	virtual bool isWhite() const;
	virtual char getName() const;

	//operators:
	bool operator==(const Piece& other) const;
	bool operator!=(const Piece& other) const;
	bool operator!() const;
};