#include "Player.h"
#include "Exceptions.h"
//Constructor
Player::Player(Board& board, const bool isWhite, King& king)
{
	this->_board = &board;
	this->_king = &king;
	this->_isWhite = isWhite;
}

Player::~Player()
{
	//Destructor: no dynamic variabels to destroy
}

//Check if king is chessed
bool Player::isChessed() const
{
	return this->_king->isChessed();
}

//Check if king is checkmated
bool Player::isCheckmated() const
{
	return this->_king->isCheckmated();
}

//returns if player is while or black
bool Player::isWhite() const
{
	return this->_isWhite;
}

/*Moves this player piece from src to dst.
* Checks if move can be made, if not, throws exception
* Input: piece to move, dst to move the piece
*/
void Player::movePiece(const int srcCol, const int srcRow, const int dstCol, const int dstRow)
{
	Piece* srcPiece = &this->_board->getPiece(srcRow, srcCol);
	Piece* dstPiece = &this->_board->getPiece(dstRow, dstCol);
	if (srcCol == dstCol && srcRow == dstRow)
	{
		throw ExceptionSameSrcDest(); //Exception: MOVING_TO_SAME_PLACE;
	}
	else if (this->isWhite() != srcPiece->isWhite() || !(*srcPiece))
	{
		throw ExceptionWrongSource(); //exception: moving other player piece
	}
	else if (!(!(*dstPiece)) && this->isWhite() == dstPiece->isWhite())
	{
		throw ExceptionSelfAttack();
	}
	//try to move
	if (!this->_board->movePiece(srcCol, srcRow, dstCol, dstRow))
	{
		throw ExceptionIllegalMove();
	}
	if (this->isChessed())
	{
		this->_board->restoreMove(srcCol, srcRow, dstCol, dstRow);
		throw ExceptionSelfChess(); //move leads to chess on this player
	}
	//moved successfully

	if (srcPiece->getName() == PAWN_NAME_WHITE || srcPiece->getName() == PAWN_NAME_BLACK)
	{
		//if pawn was moved, it's not pawns firs move anymore
		Pawn* pawn = dynamic_cast<Pawn*>(srcPiece);
		pawn->setFirstMove(false);
	}
}