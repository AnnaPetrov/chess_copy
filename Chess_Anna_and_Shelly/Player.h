#pragma once
#include "Board.h"
#include "King.h"

class Player
{
private:
	Board* _board;
	King* _king;
	bool _isWhite;

public:
	//Constructor and destructor:
	Player(Board& board, const bool isWhite, King& king);
	virtual ~Player();

	//getters:
	bool isChessed() const;
	bool isCheckmated() const;
	bool isWhite() const;

	//other:
	void movePiece(const int srcCol, const int srcRow, const int dstCol, const int dstRow);
};