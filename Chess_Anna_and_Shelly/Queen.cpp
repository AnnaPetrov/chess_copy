#include "Queen.h"
#include "Rook.h"
#include "Bishop.h"
Queen::Queen(const bool isWhite, Board& board) :
	Piece(isWhite, board)
{
	if (this->_isWhite)
	{
		this->_name = QUEEN_NAME_WHITE;
	}
	else
	{
		this->_name = QUEEN_NAME_BLACK;
	}
}

Queen::~Queen()
{
}

/*Queen can move like both bishop and rook.
* Input: src to move from, dst to move to
* Output: can queen move like this?
*/
bool Queen::isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const
{
	bool flag = true;
	if (srcCol == dstCol || srcRow == dstRow)//rooklike move
	{
		flag = Rook::isLegalMove(this->_board, srcCol, srcRow, dstCol, dstRow);
	}
	else if (std::abs(srcCol - dstCol) == std::abs(srcRow - dstRow))//bishoplike move
	{
		flag = Bishop::isLegalMove(this->_board, srcCol, srcRow, dstCol, dstRow);
	}
	else//unrecongized pattern
	{
		flag = false;
	}
	return flag;
}