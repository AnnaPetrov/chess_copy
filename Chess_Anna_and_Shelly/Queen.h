#pragma once
#include "Piece.h"

class Queen : public Piece
{
public:
	//Constructor and destructor
	Queen(const bool isWhite, Board& board);
	virtual ~Queen();
	//other
	virtual bool isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const override;
};