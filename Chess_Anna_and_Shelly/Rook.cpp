#include "Rook.h"
#include "Defines.h"
#include "Board.h"

Rook::Rook(const bool isWhite, Board& board) :
	Piece(isWhite, board)
{
	if (this->_isWhite)
	{
		this->_name = ROOK_NAME_WHITE;
	}
	else
	{
		this->_name = ROOK_NAME_BLACK;
	}
}

Rook::~Rook()
{
}

/*Checks if rook can move from given place to given place
* Rook can move only in col or row, it can't move if there is other piece on it's way
* Input: rook id and dst id
* Output: Can rook move like this?
*/
bool Rook::isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const
{
	return this->isLegalMove(this->_board, srcCol, srcRow, dstCol, dstRow);
}

//same function, but static with board as parameter
bool Rook::isLegalMove(Board* board, const int srcCol, const int srcRow, const int dstCol, const int dstRow)
{
	bool flag = true;
	int start = 0, end = 0;
	while (flag)
	{
		// check if move is made in one line
		if (srcCol != dstCol && srcRow != dstRow)
		{
			flag = false;
			break;
		}
		//check for other pieces in the way
	   // mov in row
		if (srcRow == dstRow)
		{
			start = std::min(srcCol, dstCol) + 1;//current location already checked
			end = std::max(srcCol, dstCol) - 1;// another piece in dest cell is okay
			for (size_t i = start; i <= end; i++)
			{
				if (board->getPiece(srcRow, i).getName() != NULL_PIECE_NAME)
				{
					flag = false;
					break;
				}
			}
		}
		// mov in col
		else
		{
			start = std::min(srcRow, dstRow) + 1;// current location already checked
			end = std::max(srcRow, dstRow) - 1;// another piece in dest cell is okay
			for (size_t i = start; i <= end; i++)
			{
				if (board->getPiece(i, srcCol).getName() != NULL_PIECE_NAME)
				{
					flag = false;
					break;
				}
			}
		}
		break;
	}
	return flag;
}