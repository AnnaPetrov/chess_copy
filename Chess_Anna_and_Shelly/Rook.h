#pragma once
#include "Piece.h"

class Rook : public Piece
{
public:
	Rook() = default;
	//Constructor and destructor
	Rook(const bool isWhite, Board& board);
	virtual ~Rook();

	//other
	virtual bool isLegalMove(const int srcCol, const int srcRow, const int dstCol, const int dstRow) const override;
	static bool isLegalMove(Board* board, const int srcCol, const int srcRow, const int dstCol, const int dstRow);
};